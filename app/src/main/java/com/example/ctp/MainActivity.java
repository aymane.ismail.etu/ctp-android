package com.example.ctp;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private Candidature[] candidatures;
    private int active = 0;
    private int cpt = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            candidatures = Candidature.LoadCandidaturesFromResources(this, R.raw.cand);
        } catch (JSONException | IOException e) {
            Toast failedLoad = Toast.makeText(this, R.string.failedLoad, Toast.LENGTH_LONG);
            failedLoad.show();
            candidatures = new Candidature[0];
        }

        stats();
        update();
    }

    protected void stats(){

        int nope = 0;
        int todo = 0;
        int done = 0;

        for(Candidature cand : candidatures){
            if (cand.getEtat() == Candidature.Etat.NOPE || cand.getEtat() == Candidature.Etat.LATE) {
                nope++;
            } else if (cand.getEtat() == Candidature.Etat.TODO) {
                todo++;
            } else if (cand.getEtat() == Candidature.Etat.DONE) {
                done++;
            }
        }

        TextView cptCand = findViewById(R.id.nbCand);
        cptCand.setText(""+candidatures.length);

        TextView cptTodo = findViewById(R.id.nbTodo);
        cptTodo.setText(""+todo);

        TextView cptNope = findViewById(R.id.nbNope);
        cptNope.setText(""+nope);

        TextView cptDone = findViewById(R.id.nbDone);
        cptDone.setText(""+done);

    }

    private void update() {

        Button prev = findViewById(R.id.Prev);
        Button next = findViewById(R.id.Next);

        if(active == 0){
            prev.setEnabled(false);
            prev.setBackgroundColor(Color.GRAY);
        } else {
            prev.setEnabled(true);
            prev.setBackgroundColor(Color.BLUE);
        }

        if(cpt>candidatures.length){
            next.setEnabled(false);
            next.setBackgroundColor(Color.GRAY);
        }else {
            next.setEnabled(true);
            next.setBackgroundColor(Color.BLUE);
        }

        Spinner sp = (Spinner) findViewById(R.id.lespinner);
        ArrayAdapter adTerminal = ArrayAdapter.createFromResource(this, R.array.etats,
                android.R.layout.simple_spinner_item) ;
        sp.setAdapter(adTerminal) ;

        if(candidatures[active].getEtat() == Candidature.Etat.NOPE || candidatures[active].getEtat() == Candidature.Etat.LATE){
            sp.setSelection(0);
        } else if(candidatures[active].getEtat() == Candidature.Etat.TODO){
            sp.setSelection(1);
        }else{
            sp.setSelection(2);
        }

        TextView formationName = findViewById(R.id.nom);
        formationName.setText(candidatures[active].getNom());

        TextView limiteDate = findViewById(R.id.date);
        Date date = candidatures[active].limite;
        limiteDate.setText(date.toString());
    }



        public void onClick(View v) {
            String url = candidatures[active].web;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }



    public void next(View view) {
        if (active < candidatures.length -1) {
            active++;
        }
        cpt++;
        update();
    }

    public void prev(View view) {
        if (active > 0) {
            active--;
        }
        cpt--;
        update();
    }

}