package com.example.ctp;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Candidature {

    private static DateFormat df = DateFormat.getDateInstance();
    private static Candidature[] candidatures;
    public String nom;
    public String web;
    public Etat etat;
    public Date limite;
    public String reponse;

    public enum Etat { NOPE, TODO, LATE, DONE };

    public static Candidature[] LoadCandidaturesFromResources(Context context, int resId)
            throws JSONException, IOException {
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new
                InputStreamReader(context.getResources().openRawResource(resId)));
        String line;
        while ((line = br.readLine()) != null) {
            text.append(line);
            text.append('\n');
        }
        br.close();
        JSONArray array = new JSONArray(text.toString());
        return LoadCandidatures(array);
    }

    private static Candidature[] LoadCandidatures(JSONArray json) throws JSONException {
        candidatures = new Candidature[json.length()];
        for (int i = 0; i < json.length(); i++) {
            candidatures[i] = new Candidature(json.getJSONObject(i));
        }
        return candidatures;
    }

    public Candidature(JSONObject json) throws JSONException {
        this.nom = json.getString("nom");
        this.web = json.getString("web");
        this.etat = Etat.valueOf(json.getString("etat"));
        try {
            this.limite = df.parse(json.getString("limite"));
        } catch (ParseException e) {
            this.limite = new Date();
        }
        this.reponse = json.getString("reponse");
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }
}
